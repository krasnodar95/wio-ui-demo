import 'package:intl/intl.dart';

class Money {
  final String currency;
  final int mainAmount;
  final int addAmount;

  Money({required this.currency, required this.mainAmount, this.addAmount = 0});

  String get formattedMainAmound {
    return NumberFormat('#,###').format(mainAmount);
  }

  String get formattedAddAmount {
    return NumberFormat('.00').format(addAmount / 100);
  }
}
