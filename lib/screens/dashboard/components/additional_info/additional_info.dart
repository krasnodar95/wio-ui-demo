import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wio_ui_demo/models/money.dart';
import 'package:wio_ui_demo/themes/colors.dart';
import 'package:wio_ui_demo/ui_components/money_widget.dart';

class AdditionalInfo extends StatelessWidget {
  const AdditionalInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        WioAssistantSection(),
        SizedBox(height: 33.0),
        Text(
          'Invoces',
          style: TextStyle(fontSize: 24.0),
        ),
        SizedBox(height: 22.0),
        Row(
          children: [
            TileBlock(
              money: Money(currency: "AED", mainAmount: 12674),
              svgSource: 'icons/Card.svg',
              description: 'INV-032',
              style: TileBlockStyle.magnolia,
            ),
            Spacer(),
            TileBlock(
              money: Money(currency: "AED", mainAmount: 4674),
              svgSource: 'icons/Car.svg',
              description: 'INV-032',
              style: TileBlockStyle.magnolia,
            ),
          ],
        ),
        SizedBox(height: 36.0),
        Container(
          height: 182.0,
          decoration: BoxDecoration(
            color: WioColors.sand,
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 20.0,
                left: 24.0,
                width: 180.0,
                child: Text(
                  'Saving Spaces makes your money work for you.',
                  style: TextStyle(fontSize: 24.0),
                ),
              ),
              Positioned(
                bottom: 0.0,
                right: 0.0,
                width: 178,
                child: ClipRRect(
                  child: Image.asset('images/Man.png'),
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(10.0),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}

class WioAssistantSection extends StatelessWidget {
  const WioAssistantSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 39.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: WioColors.greyLight),
        ),
      ),
      child: Column(
        children: [
          Row(
            children: [
              SvgPicture.asset('images/Logo.svg'),
              SizedBox(width: 4.0),
              Text('assistant', style: TextStyle(fontSize: 24.0)),
            ],
          ),
          SizedBox(height: 22.0),
          Row(
            children: [
              TileBlock(
                money: Money(currency: "AED", mainAmount: 500, addAmount: 62),
                svgSource: 'icons/Energy.svg',
                description: 'PAY ENERGY BILL',
                style: TileBlockStyle.dark,
              ),
              Spacer(),
              TileBlock(
                money: Money(currency: "AED", mainAmount: 420, addAmount: 90),
                svgSource: 'icons/Car.svg',
                description: 'PAY CAR INSURANCE',
                style: TileBlockStyle.porcelain,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

enum TileBlockStyle {
  porcelain,
  magnolia,
  dark,
}

class TileBlock extends StatelessWidget {
  final Money _money;
  final String _svgSource;
  final String _description;
  final TileBlockStyle _style;

  const TileBlock(
      {Key? key,
      required Money money,
      required String svgSource,
      required String description,
      required TileBlockStyle style})
      : _money = money,
        _svgSource = svgSource,
        _description = description,
        _style = style,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Color textColor;
    Color bgColor;
    switch (_style) {
      case TileBlockStyle.porcelain:
        textColor = WioColors.midnightBlue;
        bgColor = WioColors.porcelain;
        break;
      case TileBlockStyle.magnolia:
        textColor = WioColors.midnightBlue;
        bgColor = WioColors.magnolia;
        break;
      case TileBlockStyle.dark:
        textColor = Colors.white;
        bgColor = WioColors.midnightBlue;
        break;
    }

    return Container(
      padding: EdgeInsets.all(20.0),
      width: 182.0,
      height: 182.0,
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          MoneyWidget(
            money: _money,
            fontSize: 28.0,
            subFontSize: 14.0,
            color: textColor,
          ),
          Spacer(),
          SvgPicture.asset(_svgSource),
          SizedBox(height: 9.0),
          Row(
            children: [
              Text(
                _description,
                style: TextStyle(
                  color: textColor,
                  fontSize: 12.0,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
