import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wio_ui_demo/themes/colors.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 113.0,
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: WioColors.greyLight)),
      ),
      child: Row(
        children: [
          SizedBox(width: 60.0),
          SvgPicture.asset('icons/Pictos.svg'),
          SizedBox(
            width: 8.0,
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                fontSize: 36.0,
                fontFamily: 'AktivGrotesk-Regular',
              ),
              children: <TextSpan>[
                TextSpan(text: 'Good morning '),
                TextSpan(
                  text: 'Jamal',
                  style: TextStyle(color: WioColors.electricIndigo),
                )
              ],
            ),
          ),
          Spacer(),
          Container(
            width: 60.0,
            height: 60.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: WioColors.porcelain,
            ),
            child: SvgPicture.asset(
              'icons/Dialog.svg',
              fit: BoxFit.scaleDown,
            ),
          ),
          SizedBox(width: 60.0),
        ],
      ),
    );
  }
}
