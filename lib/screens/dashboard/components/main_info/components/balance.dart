import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wio_ui_demo/models/money.dart';
import 'package:wio_ui_demo/themes/colors.dart';
import 'package:wio_ui_demo/ui_components/money_widget.dart';

class Balance extends StatelessWidget {
  const Balance({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: WioColors.greyLight)),
      ),
      child: Stack(
        children: [
          Text(
            'Current balance',
            style: TextStyle(fontSize: 24.0),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: 35.0,
              bottom: 26.0,
            ),
            child: Row(
              children: [
                MoneyWidget(
                  money: Money(
                    currency: 'AED',
                    mainAmount: 50443,
                    addAmount: 45,
                  ),
                  fontSize: 60.0,
                ),
                Spacer(),
                BalanceButton(svgAssetName: 'icons/Plus.svg'),
                SizedBox(width: 12),
                BalanceButton(svgAssetName: 'icons/Right.svg'),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class BalanceButton extends StatelessWidget {
  final String _svgAssetName;

  const BalanceButton({
    Key? key,
    required String svgAssetName,
  })  : _svgAssetName = svgAssetName,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48.0,
      width: 48.0,
      decoration: BoxDecoration(
        color: WioColors.midnightBlue,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: SvgPicture.asset(
        _svgAssetName,
        fit: BoxFit.scaleDown,
      ),
    );
  }
}
