import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';

class SimpleBarChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BarChart(
      _createSampleData(),
      animate: true,
      primaryMeasureAxis: NumericAxisSpec(renderSpec: NoneRenderSpec()),
      domainAxis: OrdinalAxisSpec(renderSpec: NoneRenderSpec()),
      layoutConfig: LayoutConfig(
        leftMarginSpec: MarginSpec.fixedPixel(0),
        topMarginSpec: MarginSpec.fixedPixel(0),
        rightMarginSpec: MarginSpec.fixedPixel(0),
        bottomMarginSpec: MarginSpec.fixedPixel(0),
      ),
      defaultRenderer: BarRendererConfig(
        groupingType: BarGroupingType.grouped,
        cornerStrategy: ConstCornerStrategy(10),
      ),
    );
  }

  List<Series<UserTransaction, String>> _createSampleData() {
    return [
      Series(
          id: 'Sum',
          data: [
            UserTransaction('January', 5, Color.fromHex(code: '#0F1A38')),
            UserTransaction('February', 3, Color.fromHex(code: '#8E55FB')),
          ],
          domainFn: (UserTransaction transaction, _) => transaction.month,
          measureFn: (UserTransaction transaction, _) => transaction.amount,
          colorFn: (UserTransaction transaction, _) => transaction.color)
    ];
  }
}

class UserTransaction {
  final String month;
  final int amount;
  final Color color;

  UserTransaction(this.month, this.amount, this.color);
}
