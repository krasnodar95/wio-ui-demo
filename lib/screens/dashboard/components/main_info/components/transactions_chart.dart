import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wio_ui_demo/models/money.dart';
import 'package:wio_ui_demo/screens/dashboard/components/main_info/components/bar_chart.dart';
import 'package:wio_ui_demo/themes/colors.dart';
import 'package:wio_ui_demo/ui_components/money_widget.dart';

class TransactionsChart extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: WioColors.greyLight)),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 36.0),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  'Transactions',
                  style: TextStyle(fontSize: 24.0),
                ),
                Spacer(),
                PeriodSelector()
              ],
            ),
            Container(
              padding: EdgeInsets.only(top: 50.0, bottom: 0),
              child: SimpleBarChart(),
              height: 180.0,
            ),
            SizedBox(height: 30.0),
            TransactionInfoPanel(),
          ],
        ),
      ),
    );
  }
}

class TransactionInfoPanel extends StatelessWidget {
  const TransactionInfoPanel({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        MoneyWidget(
          money: Money(
            currency: 'AED',
            mainAmount: 61752,
            addAmount: 2,
          ),
          fontSize: 28.0,
          subFontSize: 12.0,
        ),
        SizedBox(width: 64.0),
        MoneyWidget(
          money: Money(
            currency: 'AED',
            mainAmount: 19492,
            addAmount: 63,
          ),
          fontSize: 28.0,
          subFontSize: 12.0,
        ),
        Spacer(),
        ActionButton(svgSource: 'icons/Calendar.svg'),
        SizedBox(width: 10),
        ActionButton(svgSource: 'icons/Sort.svg'),
        SizedBox(width: 10),
        ActionButton(svgSource: 'icons/Download.svg'),
      ],
    );
  }
}

class ActionButton extends StatelessWidget {
  final String _svgSource;

  const ActionButton({
    Key? key,
    required String svgSource,
  })  : _svgSource = svgSource,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48.0,
      width: 48.0,
      decoration: BoxDecoration(
        border: Border.all(
          width: 1.0,
          color: WioColors.midnightBlue,
        ),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: SvgPicture.asset(
        _svgSource,
        fit: BoxFit.scaleDown,
      ),
    );
  }
}

class PeriodSelector extends StatelessWidget {
  const PeriodSelector({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          'YEAR',
          style: TextStyle(
            fontSize: 12.0,
            color: WioColors.manatee,
          ),
        ),
        SizedBox(width: 26.0),
        Text(
          '4M',
          style: TextStyle(
            fontSize: 12.0,
            color: WioColors.manatee,
          ),
        ),
        SizedBox(width: 26.0),
        Text(
          '2M',
          style: TextStyle(
            fontSize: 12.0,
            color: WioColors.manatee,
          ),
        ),
        SizedBox(width: 26.0),
        Text(
          '1M',
          style: TextStyle(
            fontSize: 12.0,
            color: WioColors.midnightBlue,
          ),
        ),
      ],
    );
  }
}
