import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wio_ui_demo/screens/dashboard/components/main_info/components/transactions_chart.dart';
import 'package:wio_ui_demo/themes/colors.dart';

import 'components/balance.dart';

class MainInfo extends StatelessWidget {
  const MainInfo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Balance(),
        TransactionsChart(),
        Column(
          children: [
            UserTransaction(
              title: 'Salik',
              description: 'Account Top-Up',
              amount: '150.00',
            ),
            UserTransaction(
              title: 'Salik',
              description: 'Account Top-Up',
              amount: '150.00',
            ),
            UserTransaction(
              title: 'Salik',
              description: 'Account Top-Up',
              amount: '150.00',
            ),
            UserTransaction(
              title: 'Salik',
              description: 'Account Top-Up',
              amount: '150.00',
            ),
            UserTransaction(
              title: 'Salik',
              description: 'Account Top-Up',
              amount: '150.00',
            ),
            UserTransaction(
              title: 'Salik',
              description: 'Account Top-Up',
              amount: '150.00',
            ),
          ],
        )
      ],
    );
  }
}

class UserTransaction extends StatelessWidget {
  final String _title;
  final String _description;
  final String _amount;

  const UserTransaction(
      {Key? key,
      required String title,
      required String description,
      required String amount})
      : _title = title,
        _description = description,
        _amount = amount,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: WioColors.greyLight)),
      ),
      child: Row(
        children: [
          TransactionIcon(),
          SizedBox(width: 18.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(_title,
                  style: TextStyle(fontSize: 14.0, color: Colors.grey)),
              Text(_description, style: TextStyle(fontSize: 12.0)),
            ],
          ),
          Spacer(),
          Text(_amount, style: TextStyle(fontSize: 17.0)),
        ],
      ),
    );
  }
}

class TransactionIcon extends StatelessWidget {
  const TransactionIcon({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 48.0,
      height: 48.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: WioColors.greyLight,
      ),
      child: SvgPicture.asset(
        'icons/ATM.svg',
        fit: BoxFit.scaleDown,
        color: Colors.grey,
      ),
    );
  }
}
