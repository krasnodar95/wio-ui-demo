import 'package:flutter/material.dart';
import 'components/additional_info/additional_info.dart';
import 'components/header.dart';
import 'components/main_info/main_info.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Header(),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 50.0, horizontal: 60.0),
                    child: MainInfo(),
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 50.0,
                      right: 60.0,
                      bottom: 50.0,
                    ),
                    child: AdditionalInfo(),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
