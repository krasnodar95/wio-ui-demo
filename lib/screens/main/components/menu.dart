import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wio_ui_demo/themes/colors.dart';

import 'menu_item.dart';

class Menu extends StatelessWidget {
  const Menu({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(20.0),
          bottomRight: Radius.circular(20.0),
        ),
        boxShadow: [
          BoxShadow(
            color: WioColors.greyLight,
            blurRadius: 20.0,
          )
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 40.0),
          ClipRRect(
            child: Image.asset('images/Avatar.jpg'),
            borderRadius: BorderRadius.circular(54.0 / 2),
          ),
          SizedBox(height: 10.0),
          MenuItem(
            svgSource: 'icons/Notification.svg',
            press: () {},
          ),
          Spacer(),
          MenuItem(
            svgSource: 'icons/Home.svg',
            press: () {},
          ),
          MenuItem(
            svgSource: 'icons/Document.svg',
            press: () {},
          ),
          MenuItem(
            svgSource: 'icons/Expand.svg',
            press: () {},
          ),
          MenuItem(
            svgSource: 'icons/Card.svg',
            press: () {},
          ),
          MenuItem(
            svgSource: 'icons/Busy.svg',
            press: () {},
          ),
          MenuItem(
            svgSource: 'icons/Search.svg',
            press: () {},
          ),
          Spacer(),
          MenuItem(
            svgSource: 'icons/Settings.svg',
            press: () {},
          ),
          SvgPicture.asset('images/Logo.svg'),
          SizedBox(height: 40.0)
        ],
      ),
    );
  }
}
