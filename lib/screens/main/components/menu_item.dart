import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:wio_ui_demo/themes/colors.dart';

class MenuItem extends StatelessWidget {
  const MenuItem({
    Key? key,
    required this.svgSource,
    required this.press,
  }) : super(key: key);

  final String svgSource;
  // todo: Handle press
  final VoidCallback press;
  final _isDotShowed = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60.0,
      height: 60.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            svgSource,
            color: WioColors.manatee,
            width: 25.0,
            height: 25.0,
          ),
          SizedBox(
            height: 9.0,
          ),
          Container(
            width: 7.0,
            height: 7.0,
            decoration: BoxDecoration(
              color: WioColors.electricIndigo
                  .withOpacity(_isDotShowed ? 1.0 : 0.0),
              borderRadius: BorderRadius.circular(7.0 / 2),
            ),
          )
        ],
      ),
    );
  }
}
