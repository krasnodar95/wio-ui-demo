import 'package:flutter/material.dart';
import 'package:wio_ui_demo/screens/dashboard/dashboard_screen.dart';

import 'components/menu.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Row(
          children: [
            Menu(),
            DashboardScreen(),
          ],
        ),
      ),
    );
  }
}
