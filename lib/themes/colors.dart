import 'package:flutter/material.dart';

extension WioColors on Colors {
  static Color get electricIndigo => Color(0xFF5500F9);
  static Color get heliotrope => Color(0xFF8E55FB);
  static Color get magnolia => Color(0xFFF8F5FF);
  static Color get midnightBlue => Color(0xFF0F1A38);
  static Color get manatee => Color(0xFF878D9C);
  static Color get porcelain => Color(0xFFF5F6F7);
  static Color get sand => Color(0xFFEFEEE8);
  static Color get carnation => Color(0xFFEF4B68);
  static Color get greyLight => Colors.grey.shade300;
}
