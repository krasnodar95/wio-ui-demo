import 'package:flutter/widgets.dart';
import 'package:wio_ui_demo/models/money.dart';
import 'package:wio_ui_demo/themes/colors.dart';

class MoneyWidget extends StatelessWidget {
  final Money _money;
  final double _fontSize;
  final double? _subFontSize;
  final Color? _color;

  MoneyWidget(
      {required Money money,
      required double fontSize,
      double? subFontSize,
      Color? color})
      : _money = money,
        _fontSize = fontSize,
        _subFontSize = subFontSize,
        _color = color;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        text: _money.formattedMainAmound,
        style: TextStyle(
            fontSize: _fontSize, color: _color ?? WioColors.midnightBlue),
        children: <InlineSpan>[
          WidgetSpan(
            alignment: PlaceholderAlignment.top,
            child: Container(
              padding: EdgeInsets.only(top: _fontSize / 6),
              child: Text(
                _money.formattedAddAmount + ' ' + _money.currency,
                style: TextStyle(
                  fontSize: _subFontSize ?? _fontSize / 3,
                  color: _color ?? WioColors.midnightBlue,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
